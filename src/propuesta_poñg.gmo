# POÑG (Propuesta)

este texto fue enviado como parte de mi postulación para el programa jóvenes creadores (1)2020-(1)2021 del fonca, en la disciplina de medios audiovisuales, especialidad de nuevas tecnologías.

la propuesta no fue aceptada en la fase resolutiva.

# Descripción

POÑG es un videojuego digital a gran escala cuyo funcionamiento lógico es provisto por personas.

El juego es una simulación monocromática y con muy baja resolución (16x12) de un partido de tenis para dos personas jugadoras. Su funcionamiento se basa en un aparato de hardware digital diseñado con la intención de ser muy lento y relativamente amigable con el mundo vivo: en vez de requerir el uso de semiconductores para sus transistores, compuertas lógicas y/o circuitos integrados - componentes asumidos como necesarios en el mundo de la electrónica digital, y con un alto costo social y ambiental-, logra computar gracias a una o más personas operadoras, que son parte del equipo artístico y previamente preparadas para la labor.

POÑG se instala en un espacio amplio y abierto. Por un lado está la sala de juego, donde hay asientos para dos personas jugadoras del público, sus respectivas palancas de mando, y una gran pantalla de bombillas; aquí el demás público es invitado a turnarse para jugar y seguir el progreso del juego. Por otro lado está todo el aparato de hardware, transitado y manejado por las personas operadoras. Esta área parece laberinto, y aquí el público es invitado a recorrerlo y a observar la operación.

El aparato está conformado por materiales eléctricos sencillos y fáciles de reusar: cables, interruptores, y bombillas. Estos componentes están agrupados en múltiples bloques de interruptores rodeados por bombillas, y estos bloques están interconectados por un entramado complejo de cables.

Para funcionar, el juego depende de que las personas operadoras enciendan o apaguen cada uno de todos los interruptores que lo conforman, de acuerdo a una regla lógica simple.

Cuando estas personas terminan de evaluar las centenas de interruptores contenidos en el aparato, lo que han logrado es completar el procesamiento de un cuadro (frame) de la animación del videojuego, que un interruptor maestro les permite mostrar en la pantalla de bombillas de las personas jugadoras.

Con esta información en la pantalla, quienes juegan pueden elegir cambiar la posición de su palanca de mando, para después esperar el cómputo de la siguiente iteración que resultará en un cuadro (frame) distinto.

Así, jugando cuadro por cuadro y movida por movida, esperando el procesamiento de las personas operadoras, las jugadoras eventualmente (o no) podrán ganar el partido.

# Justificación

Las tecnologías de la información y de la comunicación, fundamentales para entender el estado actual de la civilización, se basan completamente en el uso de circuitos digitales fabricados con semiconductores. La miniaturización y la aceleración de estos circuitos, además de tener un gran costo ambiental, social y económico, los han convertido en literales cajas negras: se ha vuelto muy difícil el conocer y entender qué pasa en su interior, y cómo es que logran hacer emerger todas las capacidades que entendemos inherentes a estas tecnologías.

Por otro lado, la configuración social actual genera e incrementa una brecha de conocimientos y posibilidades entre unos cuantos que tienen el poder de desarrollar estas tecnologías digitales, y el resto de la población al que se le asigna únicamente un rol de consumo y de fuente de datos a extraer.

Este proyecto busca visibilizar fundamentos de los circuitos digitales, tomando una aplicación conocida -un juego- y transformando su hardware en un aparato miles de veces más grande y millones de veces más lento. Al exponer sus "tripas" y permitir el tránsito del público cerca de ellas, y al agregar en la ecuación al componente humano que ha de proveer la función lógica, se exhibe tanto literal como metafóricamente cómo es que paso a paso logra desarrollar su comportamiento: operaciones sencillas y repetitivas, realizadas por un "otro".

También, el proyecto busca invitar al público a observar el fenómeno de complejidad emergente: el hecho de que el juego surja únicamente a partir de la interconexión de múltiples operaciones sencillas y similares.

Por último, el proyecto también puede ser entendido como un ejercicio especulativo, que visualiza usos alternativos de lo eléctrico/electrónico, que imagina la posibilidad de recuperar y reutilizar desechos de infraestructura eléctrica, y que como juego lento agrega fricción al torrente contemporáneo de entretenimiento inmediato y sin fin.

El proyecto es viable desde el punto de vista técnico porque se apoya en la disciplina del diseño de hardware digital, un cuerpo de conocimientos que ha existido desde hace décadas pero que las abstracciones del software ha dejado relegado a un reducido grupo de personas.

Un año de trabajo es suficiente para desarrollar el proyecto: el proceso de creación se enfocará en el diseño digital del aparato, en el diseño y construcción de las interfases eléctricas (interruptores acompañados por bombillas, palancas de mando, pantalla de bombillas), en la configuración espacial del aparato y su red de cables, y en las posibilidades coreográficas de las personas operadoras, quienes manipularán los interruptores al "servicio" de las personas jugadoras.

# Objetivos del proyecto


* Diseñar y construir un videojuego monocromático y con muy baja resolución (16x12) de un partido de tenis para dos personas jugadoras, a nivel de compuertas lógicas {nor} de escala humana.
* Visibilizar los mecanismos de complejidad emergente en el ámbito digital, exponiendo cómo un comportamiento de videojuego conocido puede obtenerse a partir de múltiples interruptores interconectados que siguen reglas lógicas simples.
* Plantear en la práctica una postura alternativa de creación computacional cuyos valores son la lentitud, la escala humana, la transparencia, la comunidad, y el no uso de semiconductores.

# Detalle de las obras a realizar

* Medidas: 8m x 8m x 2m
* Materiales: Bombillas LED, interruptores, cables, fuente de alimentación, personas operadoras.
* Duración: Funcionamiento continuo por turnos de 2 horas por persona operadora

# Producto cultural resultante (metas númericas)

* Una presentación pública del juego funcionando durante dos horas con dos personas operadoras.
* Un ejemplar completo del aparato del juego, que consistirá en una pantalla de 16x12 (192) bombillas LED; unas tres centenas de páneles de compuertas, con un interruptor, varios LED y terminales de conexión cada uno; dos palancas de mando; señalizaciones e indicaciones impresas; soportes estructurales; cableado etiquetado y organizado.
* Un repositorio en línea que documente el proyecto, incluyendo: descripción del diseño del hardware, en diagrama y en lenguaje {verilog}; texto que explique los bloques funcionales del diseño digital; diagramas de construcción del aparato; texto que instruya cómo construir el aparato; fotografías y videos del proceso de diseño de interfases físicas; fotografías y videos de las pruebas del aparato durante las distintas etapas del proceso; fotografías y video finales.
* Una maqueta en escala 1:10 del aparato completo y funcional.
* Un video de 10 minutos que documente y resuma el funcionamiento del juego en la Muestra Escénica.
* Colección de fotos del juego en funcionamiento durante la Muestra Escénica.

# Actividades por periodo (12)

## 2020-12-01 a 2020-12-31

Actividades:

* Diseño digital del juego, a nivel de compuertas NOR, y buscando estrategias de parametrización de bloques funcionales.
* Transcripción del diseño en Verilog, lenguaje descriptor de hardware.
* Simulación y verificación del diseño.

Metas:

* Archivo de texto con descripción del hardware a nivel de compuertas NOR en el lenguaje Verilog
* Conjunto de gráficas que muestren las condiciones y resultados de la simulación del diseño.
* Archivo de texto con notas que indiquen cómo la simulación verifica que el diseño funciona como esperado.
* Archivo de texto que documente las generalidades del diseño digital del juego.

## 2021-01-01 a 2021-01-31

Actividades:

* Obtención de la red de nodos eléctricos en el diseño.
* Diagramación de la distribución del cableado e interruptores en el espacio.
* Cálculo de materiales necesarios para maqueta.

Metas:

* Imagen en SVG con el diagrama completo del cableado del hardware.
* Archivo de texto con el resumen numérico de la cantidad de compuertas, nodos eléctricos e interconexiones.
* Archivo de texto que indique el procedimiento para llegar al diagrama de cableado a partir del diseño digital.

## 2021-02-01 a 2021-02-28

Actividades:

* Compra de materiales para maqueta
* Construcción de maqueta
* Verificación del funcionamiento de la maqueta
* Preparación del primer informe

Metas:

* Una maqueta del aparato completo a escala 1:10, construida y en correcto funcionamiento.
* Envío en tiempo y forma del Primer Informe

1er informe: del 24 de febrero al 2 de marzo de 2021

## 2021-03-01 a 2021-03-31

Actividades:

* Diseño y prototipado de la interfaz de las compuertas, que consisten en bloques con interruptores y bombillas

Metas:

* Una muestra en prototipo de la interfaz de las compuertas, con colocación de los componentes y terminales eléctricas funcionales.
* Colección de fotos que muestren las iteraciones de diseño y prototipado.
* Asistencia al Primer Encuentro con maqueta y muestra de interfaz de compuerta

1er encuentro: del 18 al 21 de marzo de 2021

## 2021-04-01 a 2021-04-30

Actividades:

* Diseño y prototipado de las palancas de mando de las personas jugadoras.
* Diseño y prototipado de la pantalla de bombillas

Metas:

* Una muestra en prototipo de las palancas de mando de las personas jugadoras.
* Una muestra en prototipo de una fracción de la pantalla de bombillas, con conectores eléctricos y manera modular de ser montada.
* Colección de fotos que muestren las iteraciones de diseño y prototipado.

## 2021-05-01 a 2021-05-31

Actividades:

* Cotización y compra de materiales: cable, interruptores, bombillas, conectores, soportes.
* Construcción de las interfases de compuertas

Metas:

* Al menos 75% de todas las interfases de compuertas, construidas y funcionales con conectores eléctricos y forma definida de ser montadas.

## 2021-06-01 a 2021-06-30

Actividades:

* Construcción de las interfases de compuertas
* Construcción de las palancas de mando
* Construcción de la pantalla de bombillas
* Preparación del Segundo Informe

Metas:

* Todas las interfases de compuertas, construidas y funcionales con conectores eléctricos y forma definida de ser montadas.
* Dos palancas de mando, construidas y funcionales con conectores eléctricos y forma definida de ser montadas.
* Al menos el 50% de la pantalla de 16x12 (192) bombillas, construida a partir de 12 bloques de 4x4, funcional con conectores eléctricos y forma definida de ser montada.
* Envío en tiempo y forma del Segundo Informe

2do informe: del 30 de junio al 6 de julio de 2021

## 2021-07-01 a 2021-07-31

Actividades:

* Construcción de la pantalla de bombillas
* Conexión completa del aparato para pruebas eléctricas y de funcionamiento

Metas:

* Toda la pantalla de 16x12 (192) bombillas, construida a partir de 12 bloques de 4x4, funcional con conectores eléctricos y forma definida de ser montada.
* Un video que muestre al aparato funcionando en su totalidad
* Asistencia al Segundo Encuentro con video del aparato funcionando

2do encuentro: del 22 al 25 de julio de 2021

## 2021-08-01 a 2021-08-31

Actividades:

* Construcción completa del aparato
* Exploración coreográfica para las personas operadoras

Metas:

* Un video y una colección de fotos que muestren al aparato completo funcionando
* Un manual indicando cómo armar el aparato desde el punto de vista eléctrico y mecánico

## 2021-09-01 a 2021-09-30

Actividades:

* Producción de audios con indicaciones y ambientes sonoros para la puesta en escena
* Producción de señalizaciones y gráficos a añadir al aparato
* Ensayos de las personas operadoras

Metas:

* Conjunto de archivos de audio a ser reproducidos durante la puesta en escena
* Conjunto de señalizaciones y gráficos impresos para agregar al aparato
* Un video y una colección de fotos que muestren a dos personas operadoras haciendo funcionar al aparato durante media hora.

## 2021-10-01 a 2021-10-31

Actividades:

* Construcción del aparato completo, con señalizaciones y fuentes de sonido.
* Ensayos de las personas operadoras
* Pruebas públicas en pequeña escala ("playtesting") y ajustes
* Preparación del Tercer Informe

Metas:

* Un video y una colección de fotos que muestren a dos personas operadoras haciendo funcionar al aparato por dos horas seguidas, con personas externas jugando.
* Envío en tiempo y forma del Tercer Informe

3er informe: del 13 al 19 de octubre de 2021

## 2021-11-01 a 2021-11-30

Actividades:

* Preparación y montaje de la Muestra Escénica
* Ensayos de las personas operadoras
* Finalización de la documentación

Metas:

* Muestra de POÑG durante al menos un turno de dos horas.
* Un video y colección de fotos documentando la muestra.
* Repositorio en línea con todos los materiales del proceso y archivos multimedia de documentación.
