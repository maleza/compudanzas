# recetas veganas

recetas que solemos preparar, en remix constante

# hotcakes de chocolate

~35 min
~10-12 piezas

(remix de la receta en pag 46 del libro "cocinar vegano vol.1" de francesca carpinelli cocinavegano.com)

## ingredientes

* 2 tazas		avena (hojuela de grano entero, "old-fashioned")
* 1.5 tazas	leche de soya
* 1 cdita		polvo para hornear
* 1 cdita		bicarbonato de sodio
* 1 cdita		canela en polvo
* 1/2 cdita	sal de mar
* 3 cdas		azucar mascabado
* 2 cdas		cocoa en polvo
* 1 cda		extracto de vainilla
* 1 cdita		aceite de coco (u otro)
* 1/2 taza	chocolate amargo en trozos (opcional)

## instrucciones

* muele la avena hasta hacerla harina
* agrega el polvo para hornear, bicarbonato de sodio, sal, canela, cocoa y azucar
* mezcla bien
* poco a poco agrega leche de soya y bate hasta incorporar los ingredientes
* agrega el extracto de vainilla y bate de nuevo
* coloca sarten a fuego medio
* cuando se caliente, aceita y sirve la mezcla en partes iguales
* cuando se hagan burbujas en la superficie, agrega trozos de chocolate y voltea
* retira cuando tengan la consistencia que te agrade

(recuerda que no hay ingredientes crudos en la receta por lo que no hay riesgos por si parece que algo falta de cocerse)

## sugerencia

sirve con frutos rojos, moras, platano, mermeladas, mantequilla de cacahuate y/o de almendra.


# "leche" y "queso" de soya

preparar "leche" de soya resulta en restos llamados "okara", que puede usarse entre otras cosas para preparar un "queso"

~60 min
~1 lt de leche y 
un queso de ~120 gr

## ingredientes

solo para la "leche":

* 1/2 taza	frijol de soya seco
* 4 tazas		agua

para el queso:

* 2 cditas	levadura nutricional
* 1/2 cdita	sal
* 1/2		limón
* 1 chorrito	aceite de oliva
* 3 tazas		agua

necesitas alguna herramienta para licuar/moler, y otra para colar finamente (e.g. manta de cielo)

dos ollas y algunos recipientes, y un colador ayuda

## instrucciones

### previo:

* deja remojando el frijol de soya por unas 8 horas

### durante:

para el queso:
* empieza a hervir 3 tazas de agua 

para la leche:
* escurre el frijol de soya - al remojarse crece como hasta 1.5 tazas
* muele en la licuadora el frijol con 3 tazas de agua
* usa la manta de cielo para filtrar y exprimir el resultado dentro de una olla
  (el liquido se vuelve la leche, y lo solido es la okara)
* pon la olla de la leche a hervir, cuidando de quitarle la espuma y nata
* cuando hierva, cambia a fuego medio y espera ~20 minutos, quitando espuma y
  revolviendo en ocasiones
* deja enfriar y bebe!

para el queso:
* tan pronto el agua hierva, y ya tengas la okara, ponla en esa olla
* deja cocer a fuego medio durante 15-20 minutos
* usa el colador y la manta de cielo para filtrar el resultado (el liquido ya no
  lo usaremos, y el solido es la okara cocida)
* deja enfriar un rato
* cuando te sea posible, exprime para quitar todo el liquido que puedas
* coloca la okara cocida y "seca" en un recipiente
* sirve encima la levadura nutricional, sal, limon y aceite de oliva
* mezcla muy bien con tus manos
* compacta la mezcla para que sea "queso"
* aparentemente conviene refrigerar un rato antes de comerlo


## referencias

remix de estas recetas:
=> https://simpleveganblog.com/homemade-soy-milk/ leche de soya
=> https://youtube.com/watch?v=BU2LEYHb_qk	tofu casero (youtube)



# pastel de garbanzo

remix de "falafel al horno"

(receta en proceso de afinarse porque la hago de manera intuitiva (?))

~60 min
~2 "pasteles"

## ingredientes

### para la masa

* 1 taza garbanzo seco
* 2 jitomates medianos
* 1/2 cebolla
* 2 dientes ajo
* 1 cucharada sal
* 1 cucharadita pimienta
* 1 cucharada comino
* 1.5 tazas agua
* 1 cucharadita aceite
* al gusto cilantro
* al gusto chiles serranos

### extras:

* 1 cucharadita ajonjoli
* 1 pimiento
* 1/2 taza setas
* 1/2 taza espinaca

necesitas horno y alguna herramienta para moler/licuar

## instrucciones

### previo:

* deja remojando los garbanzos entre 8 y 12 horas (crecen como a 2.25 tazas)

### durante:

* escurre y enjuaga los garbanzos
* muele en la licuadora los garbanzos con el agua, cebolla, jitomates, ajo, sal,
  pimienta, comino, cilantro y chile(s)
* aceita un par de moldes para pan y sirve la mezcla en ellos
* opcional: deja reposar en el refrigerador por ~30 minutos
* parte los pimientos y setas en rebanaditas, coloca junto con la espinaca y
  ajonjoli en la superficie de la masa
* coloca los moldes en el horno, enciende a ~300 grados C
* espera unos 40-50 minutos, o hasta que adquieran la consistencia que desees
* apaga el horno y aprovecha el calor para seguir cociendo
* deja enfriar y sirve!

queda bien junto con limon, aguacate, y alguna salsa (en proceso de definirla)


# kombucha

(notas que uso para cada ronda)

~1 lt de kombucha

## ingredientes

* 1/2 taza	liquido iniciador + scoby
* 1 taza		agua caliente
* 1 cucharadita	te negro (o 2 sobres)
* 1/4 taza	azucar mascabado (4 cucharadas)
* 3 tazas		agua fria

## instrucciones

* prepara el te con el agua caliente
* cuela las hojas del te
* disuelve la azucar en el te
* diluye el te con el agua fria
* agrega la mezcla al liquido iniciador + scoby
* tapa con tela / filtros de cafe, y espera al menos un par de semanas
