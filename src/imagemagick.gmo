# apuntes de imagemagick

# redimensiona imagen

```
convert imagen.png -resize 100x100 imagen100.png
```

o con -scale, para no interpolar y mantener la apariencia de pixeles

```
convert imagen.png -scale 200% imagenx2.png
```

# reemplaza color por otro

reemplaza azul por blanco:

```
convert imagen.png -fill white -opaque blue salida.png
```

se puede agregar fuzz factor:

```
convert imagen.png -fuzz 45% -fill white -opaque blue salida.png
```

y para convertir a transparente:

```
convert imagen.png -fuzz 45% -transparent blue salida.png
```

# elimina metadatos exif

para imágenes jpg

```
convert foto.jpg -strip fotolimpia.jpg
```

esto re-comprime la imagen, pero puede usarse al mismo tiempo que se redimensiona

```
convert foto.jpg -strip -resize 640x480 fotolimpia_480p.jpg
```

# convierte pdf

## imagen a pdf

por default la conversión sucede en baja calidad, esto la mejora:

```
convert imagen.png -quality 100 -units PixelsPerInch -density 300x300 imagen.pdf
```

## pdf a imagen

para el caso opuesto:

```
convert -quality 100 -units PixelsPerInch -density 300x300 imagen.pdf imagen.png 
```

# reduce la cantidad de colores

deja que imagemagick elija qué colores utilizar:

```
convert imagen.png -colors 8 imagen_8.png
```

o con dithering:

```
convert imagen.png -dither FloydSteinberg -colors 8 imagen_8.png
```

en -dither se puede usar None, Riemersma, o FloydSteinberg


# genera mapas de bits

con color plano:

```
convert -size 100x100 xc:'rgb(0,255,0)' imagen_verde.png
```

con ruido:

```
convert -size 100x100 xc: +noise Random imagen_ruido.png 
```

# compone/encima imágenes

para componer una imagen con fondo transparente sobre otra que se convertirá en su fondo

```
composite -gravity center imagen_con_alpha.png fondo.png resultado.png 
```

# mosaico de imágenes

combina varias imágenes en mosaico, con margen de 0 entre ellas:

```
montage imagen1.png imagen2.png -geometry +0+0 mosaico.png
```

el parámetro geometry indica los márgenes entre imágenes, y opcionalmente sus dimensiones:

```
montage imagen1.png imagen2.png -geometry 100x100+0+0 mosaico.png
```

para asignar fondo transparente al montaje:

```
montage imagen1.png imagen2.png -geometry +0+0 -background none mosaico.png
```

# corta

para cortar una imagen a ciertas dimensiones (wxh) y cierto offset en x,y

```
convert imagen.png -crop 300x300+100+50 +repage recorte.png
```


para quitarte el borde de color constante a una imagen

```
convert imagen.png -trim +repage sinborde.png
```

TODO: notas sobre -chop
