# forth

stack-based and lightweight programming language.

it uses the {postfix} notation.

# reading

this book has a great pace and nice illustrations

=> https://www.forth.com/starting-forth starting forth book

# trying out

=> https://txt.eli.li/pb/forth/ forth editor lifted from easy forth - by eli_oat

# some words

## running 

these are some words for doing arithmetic with paces (min/km) and velocities (km/hr)

``` forth words
( running.forth )
: ms>s swap 60 * + ; ( min segs -- segs )
: hms>s ms>s swap 3600 * + ; ( hr min segs -- segs )
: msd>s1k >r ms>s 1000 swap r> */ ; ( min segs metros -- segs1km )
: s1k>vel 3600 swap / ; ( segs1km -- vel )
: paso>vel ms>s s1k>vel ; ( min segs -- vel )
: vel>s1k 3600 swap /  ; ( vel -- segs1km )
: ms- ms>s >r ms>s r> - ; ( min segs min segs -- segs )
: .min 0 <# [CHAR] ' HOLD #S #> TYPE ; ( min -- )
: .segpad 0 <# [CHAR] " HOLD # # #> TYPE ; ( segs -- )
: .seg 0 <# [CHAR] " HOLD #S  #> TYPE ; ( segs -- )
: .ms 60 /mod dup 0> IF .min .segpad ELSE drop .seg THEN ; ( segs -- )
: .vel . ." km/hr " ;
: lista-velocidades cr 21 10 do i dup .vel vel>s1k .ms cr loop ;
```

for example, to convert a velocity to a pace, and print it:

``` input: 18, output: 3'20"
18 vel>s1k .ms 

output:
3'20" ok
```

to do the opposite operation:

``` input: 3 20, output: 18 km/hr
3 20 ms>s s1k>vel .vel

output:
18 km/hr ok
``` 

to get the pace of a given segment, using minutes, seconds and distance in meters:

``` input: 1 03 300, output: 3'30"
( 1'03" in 300m )
1 03 300 msd>s1k .ms

output:
3'30" ok
```

to get the difference between two times in minutes, seconds:

``` input: 3 20 3 15, output: 0'05"
( 3'20" - 3'15" )
3 20 3 15 ms- .ms 

output:
5" ok
```
