# apuntes de ffmpeg

notas de usos comunes que le doy a ffmpeg

# "trim" / cortar

```
ffmpeg -i input.video -ss 5 -t 20 output.video
```

donde:
-ss es el tiempo inicial
-t es la duracion total del nuevo video


# escalar

```
ffmpeg -i input.video -vf scale=640:480 output.video
```

y para reducir el tamaño del archivo, suele ser útil recomprimir con crf (constant rate factor)

```
ffmpeg -i input.video -crf 24 output.video
```

en teoría 24 es una compresión difícil de percibir. números mayores comprimen más.

# cambio de framerate

```
ffmpeg -i input.video -r 30 output.video
```

o

```
ffmpeg -i input.video -filter:v fps=fps=30 output.video
```

# cambio de "velocidad"

obtiene un video a 2x de velocidad:

```
ffmpeg -i input.video -filter:v "setpts=0.5*PTS" output.video
```

para hacer 60x (por ejemplo, convertir minutos a segundos):

```
ffmpeg -i input.video -filter:v "setpts=0.016*PTS" output.video
```

# concatenar

hacer lista de videos con el formato:

```
file 'archivo1'
file 'archivo2'
file 'archivo3'
```
y luego:

```
ffmpeg -f concat -i lista.txt -c copy output.video
```


# generar video desde serie de img

```
ffmpeg -framerate 30 -i frame-%04d.tif -pix_fmt yuv420p -c:v libx264 <output.mp4>
```

# crop (y exportar desde recordmydesktop)

```
ffmpeg -i out.ogv -filter:v "crop=1280:720:0:0" -codec:v libx264 -codec:a libmp3lame output.mp4
```


#  video a partir de una imagen

```
# video de una imagen, 
# 10 segundos, 30 frames por segundo
ffmpeg -loop 1 \
	-i imagen_verde.png \
	-t 00:00:10 -r 30 \
	video_verde.ogv
```

# video a partir de serie de imágenes

```
# cambia entre imágenes a 2fps,
# video final a 30fps
ffmpeg -framerate 2 \
       -i "%02d.png" \
       -pix_fmt yuv420p \
       -c:v libx264 \
       -r 30 salida.mp4
```

# overlay de imagen sobre video

```
ffmpeg -i input.mp4 -i imagen.png -filter_complex "overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2" output.mp4
```
