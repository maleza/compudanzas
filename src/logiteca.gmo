# logiteca

compendio de circuitos lógicos que pueden implementarse en las {danzas compuertas}.

# acerca de

los circuitos lógicos en este compendio están descritos en {verilog}, un lenguaje descriptor de hardware. esto con la idea de estandarizarlos, de facilitar simularlos e implementarlos en otros materiales, y de indicar su cualidad de red abstracta.

cada circuito indica un número de participantes que varía entre dos opciones. el número menor está calculado como el número de entradas más el número de compuertas. el otro número suma también el número de salidas del circuito completo, que de otra forma serían realizadas por alguna(s) de las compuertas. 

utilizamos el lenguaje a nivel de {compuertas} lógicas solamente: cada compuerta se expresa como una función (and(), not(), or(), nor(), nand(), etc) donde el primer argumento es el nombre de la salida de la compuerta, y el o los otros argumentos son los nombres de las entradas.

específicamente, los circuitos están clasificados como los que usan compuertas habituales, o los que están construidos utilizando únicamente compuertas {nor}.

este es un compendio de circuitos lógicos que pueden implementarse en las {danzas compuertas}.

# multiplicador de 1 bit

circuito que multiplica a dos números de 1 bit y da el resultado en 1 bit.

## multiplicador de 1 bit con compuertas habituales (3 o 4 participantes)

```
// multiplicador construido con compuertas habituales
// entradas (2): a,b
// salidas (1): r (resultado)
// compuertas (1): 1 de 2 entradas

module multiplicador( a, b, r);

input wire a, b;
output wire r;

and C1(r, a, b);

endmodule
```

## multiplicador de 1 bit con nor (5 o 6 participantes)

=> ./img/dibujo_and-con-nor.png diagrama de interconexión mostrando a cinco personas conformando una compuerta AND a partir de compuertas NOR

```
// multiplicador construido con nor's
// entradas (2): a,b
// salidas (1): r (resultado)
// compuertas (3): 2 de 1 y 1 de 2 entradas

module multiplicador( a, b, r);

input wire a, b;
output wire r;
wire p1,p2;

// negadas
nor C1(p1, a);
nor C2(p2, b);

// resultado
nor C3(r, p1, p2);

endmodule
```

# contadores

circuitos que incrementan 1 al número binario colocado en la entrada, con el mismo número de bits a la entrada que a la salida.

si la salida se "conecta" a la entrada, entonces con cada paso se obtiene el
número siguiente en la secuencia, es decir se "cuenta".

al llegar al último número, se regresa a 0.

## contador de 2 bits con compuertas habituales (4 o 6 participantes)

```
// contador de 2 bits con compuertas habituales
// entradas (2): a, b
// salidas (2):  x, y
// compuertas (2): 1 de 1 y 1 de 2 entradas

module contador(a, b, x, y);

input wire a,b; 
output wire x,y;

not C1(y, b);
xor C2(x, a,b);

endmodule
```

## contador de 2 bits con nor (7 o 9 participantes)

```
// contador de 2 bits con nor 
// entradas (2): a, b
// salidas (2):  x, y
// compuertas (5): 2 de 1 y 3 de 2 entradas

module contador(a, b, x, y);

input wire a,b; 
output wire x,y;
wire p1, p2, p3;

// negaciones
nor C1(y, b);
nor C2(p1, a);

// para x
nor C3(p2, a,b);
nor C4(p3, y,p1);
nor C5(x, p2,p3);

endmodule
```


## contador de 3 bits con nor (13 o 16 participantes)

```
// contador de 3 bits con compuertas habituales
// entradas (3): a, b, c
// salidas (3):  x, y, z
// compuertas (10): 3 de 1, 4 de 2, 3 de 3 entradas

module contador(a, b, c,  x, y, z);

input wire a,b,c; 
output wire x,y,z;
wire p1, p2, p3, p4, p5, p6, p7;

// negaciones
nor C1(p1, a);
nor C2(p2, b);
nor C3(z, c);

// para y
nor C4(p3, b,c);
nor C5(p4, p2,z);
nor C6(y, p3,p4);

// para x
nor C7(p5, a,b);
nor C8(p6, p1, p2, z);
nor C9(p7, a, p2, c);
nor C10(x, p5, p6, p7);

endmodule
```


# half adder

circuito que suma dos bits y da el resultado en dos bits (resultado y *carry*)

## half adder con compuertas habituales (4 o 6 participantes)

```
// half adder construido con compuertas habituales
// entradas (2): a,b
// salidas (2): r, c (resultado, carry)
// compuertas (2): 2 de 2 entradas

module halfadder( a, b, r, c);

input wire a, b;
output wire r,c;

// carry
and C1(c, a, b);

// resultado
xor C2(r, a, b);

endmodule
```

## half adder usando nor (7 o 9 participantes)

=> ./img/dibujo_halfadder-con-nor.png diagrama de interconexión mostrando a siete personas conformando un half-adder a partir de compuertas NOR

```
// half adder construido usando nor's
// entradas (2): a,b
// salidas (2): r, c (resultado, carry)
// compuertas (5): 2 de 1 y 3 de 2 entradas

module halfadder( a, b, r, c);

input wire a, b;
output wire r,c;
wire p1,p2,p3;

// negadas
nor C1(p1, a);
nor C2(p2, b);

// carry
nor C3(c, p1, p2);

// resultado
nor C4(p3, a, b);
nor C5(r, c, p3);

endmodule
```


# full adder

circuito que suma tres bits y da el resultado en dos bits (resultado y *carry*).

## full adder con compuertas habituales (14 o 16 participantes)

```
// full adder construido con compuertas habituales
// entradas (3): a, b, c
// salidas (2): carry, r (resultado)
// compuertas (11): 2 de 1 y 9 de 2 entradas

module fulladder( a, b, c, r, carry);

input wire a, b, c;
output wire r, carry;
wire p1, p2, p3, p4, p5, p6, p7, p8, p9;

// negadas
not C1(p1, a);
not C2(p3, p2);

// diferencias b,c
xor C3(p2, b, c);
or C4(p4, b, c);
and C5(p5, b, c);

// armado de resultado
and C6(p6,  p1, p2);
and C7(p7,  a, p3);
or C8(r, 	 p6, p7);

// armado de carry
and C9(p8,  p1, p5);
and C10(p9,  a, p4);
or C11(carry,   p8, p9);

endmodule
```

## full adder usando nor (15 o 17 participantes)

=> ./img/dibujo_20190715-norpalera-fulladder_blanco_small.png logic circuit in the shape of nopal

computadora no(r)palera de {coloring computers}

```
// full adder construido usando nor's
// entradas (3): a, b, c
// salidas (2): carry, r (resultado)
// compuertas (12): 3 de 1, 3 de 2, 4 de 3 y 1 de 4 entradas

module fulladder( a, b, c, r, carry);

input wire a, b, c;
output wire r, carry;
wire p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;

// negadas
nor C1(p1, a); 
nor C2(p2, b);
nor C3(p3, c);

// pa'l carry
nor C4(p4, a, b);
nor C5(p5, a, c);
nor C6(p6, b, c);

// pa resultado
nor C7(p7, a, b , c);
nor C8(p8, p1, b, p3);
nor C9(p9, a, p2, p3);
nor C10(p10, p1, p2, c);

// salidas
nor C11(carry, p4, p5, p6);
nor C12(r, p7, p8, p9, p10);

endmodule
```
