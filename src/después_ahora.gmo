# después ahora

textos hacia una práctica post-civilización.

# La invitación

Primero que nada, te invito, nos invito, a estar presentes.

Respirar profundo, y de ser posible, mirar por alguna ventana. Notar el espacio de posibilidades que nos presenta lo que llamamos cielo. 

Espero que no fuera hace mucho la vez más reciente que le habías visto.

Ahora bien, en este texto, partimos de la noción de que algo grande se está terminando. No solo grande, sino enorme; tal vez inabarcable, tal vez inconcebible.

Partimos de la noción de que la civilización se está terminando. 

O de que ya se terminó, según desde dónde le veamos.

Partimos de que vale la pena permitirse sentirlo y expresarlo. La intención es imaginar, encontrar, crear cómo queremos vivir mientras lo aceptamos.

Por un lado siento las ganas de argumentarlo, de convencerte. Pero por otro lado sé que ya lo sabes: no se ve salida al desastre exponencial que se ha (¿hemos? ¿han?) desencadenado.

Crecer por crecer y por siempre, dejando de lado los cuidados: probablemente no fue la mejor idea, forma de vivir impuesta. Hay y hubo quien se opuso, claro; pero ahí no se permiten alternativas.

La duda es si esperamos y dejamos que la civilización como la conocemos, eso que se está terminando, caiga, colapse por su propio peso. A mí, esa espera me parece muy riesgosa. ¿Qué tal si al dejarle continuar, antes cae, colapsa la biósfera y su capacidad para mantener vida?

¿Cuál de los dos finales nos resultará peor, el de la civilización o el de la vida en la Tierra? Los páneles solares y autos eléctricos no nos llevan a una tercera opción, realmente.

¿Y entonces? ¿La decisión es retirarse? ¿La decisión es tirarla? ¿Qué tal vivir como si ya no existiera?

Cualquier opción me suena incómoda, pero llena de energía vital.

Personalmente, prefiero dejármelo sentir. No me va eso de hacerme creer que no está pasando nada, de hacer como si todo fuera a seguir normal, bien, "sin problema".

Ya no me creo el cuento del progreso, de las soluciones tecnocéntricas.

Vivir como si la civilización ya no existiera es lo que mejor me suena. Requiere creatividad, curiosidad, disposición, compasión y paciencia. Es alcanzable. No hay una fórmula específica, homogénea. 

Es plantar semillas de lo que pueda venir después: puede que no germinen, ¿pero realmente me veo haciendo otra cosa?

Me queda claro que no es fácil. Además, no tengo mucha idea de cómo.

Estas letras llegan a ti con miras a que exploremos, sintamos, conectemos, practiquemos.

Por el momento usamos internet, después ya veremos. Si los cables seguirán plantados, ¿qué decidiremos hacer con ellos?

---

Te invito a respirar de nuevo. A moverte con cómo te sientes ahora. 

Te invito a acompañarnos en esta búsqueda. 

Está bien si todavía no es el momento. Ya sabes dónde me encuentro.
